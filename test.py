"""
import kosapi
import pprint
import requests
from kosapi.entity.enum import Permission, ParallelType


def str_course(link):
    return link.split("/")[-2]


k = kosapi.KosapiClient()

username = "peckato1"
for stud in k.get().students(username):
    print(stud.username, stud.firstName, stud.lastName)

    for n in range(13, 18+1):
        for n2 in [1,2]:
            sem = "B" + str(n) + str(n2)
            print("sem {}".format(sem))
            for course in k.get().students(username).enrolledCourses(sem=sem):
                print("\t", str_course(course.course.link), course.course.link, course.completed, course.assessment)

"""
import kosapi

completed = list()
k = kosapi.KosapiClient()
for stud in k.get().courses('BI-PS1').students(limit=1000):
    print(stud.username, stud.code)
    for ce in k.get().students(stud.code).enrolledCourses(sem='B181'):
        if ce and ce.course and ce.course.link and 'PS1' in ce.course.link:
            if ce.completed:
                completed.append(stud.code)

print(len(completed))
