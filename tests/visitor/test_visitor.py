import unittest

from kosapi.utils import visitor


class DummyVisitableBase(visitor.Visitable): pass
class DummyVisitable1(DummyVisitableBase): pass
class DummyVisitable2(DummyVisitableBase): pass
class DummyVisitable3(DummyVisitable2): pass

class DummyVisitableBase2(visitor.Visitable): pass

class DummyNotVisitable: pass
class DummyNotVisitable2:
    def accept(self, visitor_obj, *args):
        return visitor_obj.visit(self, *args)


# noinspection PyUnusedLocal,PyUnusedLocal
class DummyVisitor1(visitor.Visitor):
    def visit_ret_test(self, item, *args):
        if args:
            return (item.__class__.__name__, *args)
        return item.__class__.__name__

    def visit_DummyVisitableBase(self, item, *args):
        return self.visit_ret_test(item, *args)

    def visit_DummyVisitable1(self, item, *args):
        return self.visit_ret_test(item, *args)

    def visit_DummyVisitable2(self, item, *args):
        return self.visit_ret_test(item, *args)

    def visit_DummyVisitable3(self, item, *args):
        return self.visit_ret_test(item, *args)

    def visit_DummyNotVisitable2(self, item, *args):
        return False


class DummyVisitor2(DummyVisitor1):
    VISITED_CLASS = DummyVisitableBase


class TestVisitor(unittest.TestCase):
    def setUp(self):
        self.v = [DummyVisitableBase(), DummyVisitable1(), DummyVisitable2(), DummyVisitable3()]
        self.v2 = [DummyVisitableBase2()]
        self.nv = [DummyNotVisitable(), DummyNotVisitable2()]

    def test_visit_accept(self):
        visitor_obj = DummyVisitor1()
        for i in range(len(self.v)):
            self.assertEqual(self.v[i].accept(visitor_obj), self.v[i].__class__.__name__)

    def test_visit_accept_type_check(self):
        visitor_obj = DummyVisitor2()
        for i in range(len(self.v)):
            self.assertEqual(self.v[i].accept(visitor_obj), self.v[i].__class__.__name__)

        for i in range(len(self.v2)):
            with self.assertRaisesRegex(TypeError, "Visitor must visit subclasses of '<.*DummyVisitableBase'>'.* 'DummyVisitableBase2'$"):
                self.v2[i].accept(visitor_obj)

        with self.assertRaisesRegex(AttributeError, "'.*' object has no attribute 'accept'$"):
            self.nv[0].accept(visitor_obj)

        with self.assertRaisesRegex(TypeError, "^Visited item is not instance of of Visitable$"):
            self.nv[1].accept(visitor_obj)

    def test_visit_arguments(self):
        visitor_obj = DummyVisitor1()

        for i in range(len(self.v)):
            self.assertEqual(self.v[i].accept(visitor_obj, 1, 2, 3), (self.v[i].__class__.__name__, 1, 2, 3))
