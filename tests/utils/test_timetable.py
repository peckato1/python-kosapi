from unittest import TestCase
import datetime

from kosapi.utils.timetable import TimetableSlotUtils


class TestTimetable(TestCase):
    def setUp(self):
        self.tu = TimetableSlotUtils()

        self.tu.add_faculty(TimetableSlotUtils.FacultyTimes(
            TimetableSlotUtils.Faculty.FIT,
            ["7:30", "8:15", "9:15", "10:00", "11:00", "11:45", "12:45", "13:30", "14:30", "15:15", "16:15", "17:00", "18:00", "18:45", "19:45"],
            ["7:30", "8:15", "9:00", "10:00", "10:45", "11:45", "12:30", "13:30", "14:15", "15:15", "16:00", "17:00", "17:45", "18:45", "19:30"]))

        self.tu.add_faculty(TimetableSlotUtils.FacultyTimes(
            TimetableSlotUtils.Faculty.FEL,
            ["7:30", "8:15", "9:15", "10:00", "11:00", "11:45", "12:45", "13:30", "14:30", "15:15", "16:15", "17:00", "18:00", "18:45", "19:45"],
            ["7:30", "8:15", "9:00", "10:00", "10:45", "11:45", "12:30", "13:30", "14:15", "15:15", "16:00", "17:00", "17:45", "18:45", "19:30"]))

    def test_facultyTimes_init(self):
        times_datetime = [datetime.time(7, 30), datetime.time(8,15)]
        times_str = [d.strftime("%H:%M") for d in times_datetime]
        f = TimetableSlotUtils.FacultyTimes(TimetableSlotUtils.Faculty.FIT, times_str, times_str)

        for i in range(0, len(f.starts)):
            self.assertEqual(times_datetime[i], f.starts[i])
            self.assertEqual(times_datetime[i], f.ends[i])

    def test_faculty_to_enum(self):
        t = TimetableSlotUtils()

        enum_values = {e.name: e.value for e in TimetableSlotUtils.Faculty }
        enum_min = min([v for k, v in enum_values.items()])
        enum_max = max([v for k, v in enum_values.items()])

        # test by F<id> string
        for i in range(enum_min - 1, enum_max + 2):
            try:
                self.assertEqual(TimetableSlotUtils.Faculty(i), t._faculty_to_enum("F%d" % i))
            except ValueError as e:
                self.assertRegex(str(e), "%s is not a valid Faculty" % i)

        # test by <id> integer -> handled by Enum class
        # test by <name> string -> handled by Enum class