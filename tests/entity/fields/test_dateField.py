import unittest
import datetime

from kosapi.entity import BaseEntity
from kosapi.entity.fields import DateField


class DummyEntity(BaseEntity):
    f = DateField()


class TestDateField(unittest.TestCase):
    def setUp(self):
        self.e = DummyEntity()

    def test__constraints_check(self):
        try:
            self.e.f = datetime.date.today()
        except ValueError:
            self.fail()

    def test__type_check(self):
        try:
            self.e.f = datetime.date.today()
        except TypeError:
            self.fail()

        with self.assertRaises(TypeError):
            self.e.f = "1970-01-01"

        with self.assertRaises(TypeError):
            self.e.f = datetime.datetime.now()
