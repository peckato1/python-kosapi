import enum
import unittest

from kosapi.entity import BaseEntity
from kosapi.entity.enum import BaseEnum
from kosapi.entity.fields import EnumField


class DummyEnum(BaseEnum):
    ITEM_ONE = 1
    ITEM_TWO = 2


class DummyEntity(BaseEntity):
    f = EnumField(enum_type=DummyEnum)


class TestEnumField(unittest.TestCase):
    def setUp(self):
        self.e = DummyEntity()

    def test__init(self):
        with self.assertRaisesRegex(TypeError, "Enum type in EnumField must be subclass of BaseEnum"):
            class InvalidDummyEnum(enum.Enum):
                pass

            class InvalidEntity(BaseEntity):
                f = EnumField(enum_type=InvalidDummyEnum)
            ie = InvalidEntity()

    def test__constraints_check(self):
        try:
            self.e.f = DummyEnum.ITEM_ONE
            self.e.f = DummyEnum.ITEM_TWO
        except ValueError:
            self.fail()

    def test__type_check(self):
        try:
            self.e.f = DummyEnum.ITEM_ONE
            self.e.f = DummyEnum.ITEM_TWO
        except TypeError:
            self.fail()

        with self.assertRaises(TypeError):
            class DummyEnum2(BaseEnum):
                ITEM_THREE = 3

            self.e.f = DummyEnum2.ITEM_THREE
