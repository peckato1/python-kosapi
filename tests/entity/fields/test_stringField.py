import unittest

from kosapi.entity import BaseEntity
from kosapi.entity.fields import StringField


class DummyEntity(BaseEntity):
    f0 = StringField()
    f1 = StringField(max_len=0)
    f2 = StringField(max_len=10)


class TestStringField(unittest.TestCase):
    def setUp(self):
        self.e = DummyEntity()

    def test__init(self):
        with self.assertRaises(ValueError):
            class InvalidEntity(BaseEntity):
                f = StringField(max_len=-1)
            ie = InvalidEntity()

        with self.assertRaises(TypeError):
            class InvalidEntity(BaseEntity):
                f = StringField(max_len="len")
            ie = InvalidEntity()


    def test__constraints_check(self):
        try:
            self.e.f0 = "0" * 1024
            self.e.f1 = ""
            self.e.f2 = "x" * 10
        except ValueError:
            self.fail()

        with self.assertRaises(ValueError):
            self.e.f1 = "1" * 1

        with self.assertRaises(ValueError):
            self.e.f2 = "1" * 11

    def test__type_check(self):
        try:
            self.e.f0 = "Hello world!"
        except TypeError:
            self.fail()

        with self.assertRaises(TypeError):
            self.e.f0 = 1
