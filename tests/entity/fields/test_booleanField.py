import unittest

from kosapi.entity import BaseEntity
from kosapi.entity.fields import BooleanField


class DummyEntity(BaseEntity):
    f = BooleanField()


class TestBooleanField(unittest.TestCase):
    def setUp(self):
        self.e = DummyEntity()

    def test__constraints_check(self):
        try:
            self.e.f = True
            self.e.f = False
        except ValueError:
            self.fail()

    def test__type_check(self):
        try:
            self.e.f = True
            self.e.f = False
        except TypeError:
            self.fail()

        with self.assertRaises(TypeError):
            self.e.f = "asd"
