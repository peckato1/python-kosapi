import unittest

from kosapi.entity.fields import BaseField, DescriptorAutoNameMeta


class DummyField1(BaseField):
    def __init__(self):
        super().__init__(type=int)
        self._list = list(range(10))

    def _constraints_check(self, val):
        return val in self._list


class DummyField2(BaseField):
    def __init__(self):
        super().__init__(type=str)

    def _constraints_check(self, val):
        return "o" in val


class DummyModel(metaclass=DescriptorAutoNameMeta):
    field1 = DummyField1()
    field2 = DummyField2()


class TestBaseField(unittest.TestCase):
    def setUp(self):
        self.m = DummyModel()

    def test__type_check(self):
        self.m.field1 = 5
        self.m.field2 = "Hello world!"

        self.assertEqual(self.m.field1, 5)
        self.assertEqual(self.m.field2, "Hello world!")

    def test__constraints_check(self):
        try:
            self.m.field2 = "o" * 10
            self.m.field2 = "o" * 20
        except ValueError:
            self.fail()

        with self.assertRaises(ValueError):
            self.m.field1 = 10
            self.m.field2 = "1" * 21
