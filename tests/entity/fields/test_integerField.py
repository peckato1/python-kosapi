import unittest

from kosapi.entity import BaseEntity
from kosapi.entity.fields import IntegerField


class DummyEntity(BaseEntity):
    f0 = IntegerField()
    f1 = IntegerField(min_val=-5)
    f2 = IntegerField(max_val=10)
    f3 = IntegerField(min_val=1, max_val=10)


class TestIntegerField(unittest.TestCase):
    def setUp(self):
        self.e = DummyEntity()

    def test__init(self):
        with self.assertRaises(TypeError):
            class InvalidEntity(BaseEntity):
                f = IntegerField(max_val="val")
            ie = InvalidEntity()

        with self.assertRaises(TypeError):
            class InvalidEntity(BaseEntity):
                f = IntegerField(min_val="val")
            ie = InvalidEntity()


    def test__constraints_check(self):
        try:
            self.e.f0 = -5
            self.e.f0 = -4
            self.e.f1 = 10
            self.e.f1 = 9

            for i in range(1, 10 + 1):
                self.e.f3 = i

        except ValueError:
            self.fail()

        with self.assertRaises(ValueError):
            self.e.f1 = -6

        with self.assertRaises(ValueError):
            self.e.f2 = 11

        with self.assertRaises(ValueError):
            self.e.f3 = 0

        with self.assertRaises(ValueError):
            self.e.f3 = 11

    def test__type_check(self):
        try:
            self.e.f0 = 3
        except TypeError:
            self.fail()

        with self.assertRaises(TypeError):
            self.e.f0 = True

        with self.assertRaises(TypeError):
            self.e.f0 = 3.14
