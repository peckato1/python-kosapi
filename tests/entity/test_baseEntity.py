import unittest

from kosapi.entity import BaseEntity
from kosapi.entity.fields import StringField


class DummyEntity(BaseEntity):
    f1 = StringField()
    f2 = StringField()


# noinspection PyUnusedLocal,PyUnusedLocal
class TestBaseEntity(unittest.TestCase):
    def setUp(self):
        self.kwargs = {'f1': 'BI-AAG', 'f2': '[1] Foo\n[2] Bar\n' }
        self.e = DummyEntity(**self.kwargs)

    def test_init(self):
        self.assertEqual(self.e.f1, self.kwargs['f1'])
        self.assertEqual(self.e.f2, self.kwargs['f2'])

    def test_getitem(self):
        self.assertEqual(self.e['f1'], self.kwargs['f1'])
        self.assertEqual(self.e.f1, self.e['f1'])

        with self.assertRaises(AttributeError):
            x = self.e["asd"]

        with self.assertRaises(AttributeError):
            x = self.e.asd


    def test_setitem(self):
        f1 = 'BI-AG1'
        self.e['f1'] = f1

        self.assertEqual(self.e['f1'], f1)
        self.assertEqual(self.e['f1'], self.e.f1)
