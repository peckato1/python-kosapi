import kosapi
import czech_sort

def get_list(course, sem):
    res = list()
    k = kosapi.KosapiClient()
    for stud in k.get().courses(course).students(limit=1000):
        code = "--"
        for p in k.get().students(stud.code).parallels(sem=sem, query="parallelType==TUTORIAL;course=={}".format(course)):
            code = p.code

        tmp = [stud.lastName, stud.firstName, stud.username, stud.email, str(code)]
        res.append(':'.join(tmp))

    return czech_sort.sorted(res)


sem = "B211"
for course in ["BIK-AAG", "BIE-AAG", "BI-AAG"]:
    with open('{}-dump_{}.csv'.format(sem, course), 'w') as f:
        for e in get_list(course, sem):
            f.write(e + "\n")

