import kosapi
import pprint, requests
from kosapi.entity.enum import Permission, ParallelType

k = kosapi.KosapiClient()

# ---------------------------------------------------------------------------------------------------------------

COURSE = "BI-AG1"
CSEM = "B181"
PSEM = "B172"

# ---------------------------------------------------------------------------------------------------------------

stats = dict()
i = 0

for stud in k.get().courses(COURSE).students(sem=CSEM, limit=100):
    i += 1
    print(i)

    try:
        pcode = None
        for parallel in k.get().students(stud.username).parallels(sem=CSEM, limit=50):
            if parallel.course.link.split("/")[1] == COURSE and parallel.parallelType == ParallelType.TUTORIAL:
                pcode = parallel.code

        courses_comp = list()
        courses_incomp = list()

        for enrollment in k.get().students(stud.username).enrolledCourses(sem=PSEM):
            if enrollment.completed:
                courses_comp.append(enrollment.course.link.split("/")[1])
            else:
                courses_incomp.append(enrollment.course.link.split("/")[1])

        stats[stud.username] = {'enrolled': pcode, 'complete': courses_comp, 'incomplete': courses_incomp}
    except requests.exceptions.HTTPError:
        print("Student order:%d username=%s not found" % (i, stud.username))
        continue

pprint.pprint(stats)