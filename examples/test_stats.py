import kosapi
import enum
import pprint, requests
from kosapi.entity.enum import Permission, ParallelType


def str_course(link):
    return link.split("/")[-2]

k = kosapi.KosapiClient()

# ---------------------------------------------------------------------------------------------------------------
students = dict()

# get all students

#courses = ["BI-PA2", "BI-LIN", "BI-SAP", "BI-DBS"]
courses = {"BI-PA2": "B172", "BI-PA1": "B171"}
"""
for course, sem in courses.items():
    for stud in k.get().courses(course).students(sem=sem, limit=250):
        students[stud.username] = dict()

# status
for stud in students:
    student_record = {course: {'study': False, 'assessment': None, 'completed': None} for course in courses.keys()}

    try:
        for course, semester in courses.items():
            for courseEnrollment in k.get().students(stud).enrolledCourses(sem=semester, limit=70):
                if courseEnrollment.course is None:
                    continue
                crs = str_course(courseEnrollment.course.link)
                try:
                    if crs == course:
                        print(stud, course, courseEnrollment.assessment, courseEnrollment.completed)
                        student_record[course]['study'] = True
                        student_record[course]['completed'] = courseEnrollment.completed
                        student_record[course]['assessment'] = courseEnrollment.assessment
                except AttributeError:
                    pass
        students[stud] = student_record
        print(student_record)
    except requests.exceptions.HTTPError:
        students[stud] = None


pprint.pprint(students, width=100)
"""

"""
import json, itertools
with open('studs.json') as f:
    x = json.loads(f.read())
    cours = {c : set() for c in courses}

    for stud, val in x.items():
        if val != "None":
            for course in courses:
                if val[course]['completed'] == 'True':
                    cours[course].add(stud)
    for i in range(1, len(courses) + 1):
        for comb in itertools.combinations(courses, i):
            print(comb, end=':')
            res = None
            for c in comb:
                if res is None:
                    res = cours[c]
                else:
                    res = res.intersection(cours[c])
            print(len(res))
"""

import json, itertools
with open('studs_pa.json') as f:
    x = json.loads(f.read())
    catg = {
        ('BI-PA1', 'NEZAPS'): set(), ('BI-PA1', 'N'): set(), ('BI-PA1', 'ZAPO'): set(), ('BI-PA1', 'A-E'): set(),
        ('BI-PA2', 'NEZAPS'): set(), ('BI-PA2', 'N'): set(), ('BI-PA2', 'ZAPO'): set(), ('BI-PA2', 'A-E'): set(),
    }

    for stud, val in x.items():
        if val == "None":
            continue

        for cours in courses.keys():
            if val[cours]['study'] == "False":
                catg[(cours, 'NEZAPS')].add(stud)
            elif val[cours]["assessment"] == "False":
                catg[(cours, 'N')].add(stud)
            elif val[cours]["assessment"] == "True" and val[cours]["completed"] == "False":
                catg[(cours, 'ZAPO')].add(stud)
            else:
                catg[(cours, 'A-E')].add(stud)

    pprint.pprint(catg)

    pa1_catg = [c for c in catg.keys() if c[0] == 'BI-PA1']
    pa2_catg = [c for c in catg.keys() if c[0] == 'BI-PA2']
    for k, v in catg.items():
        print(k, len(v))
    for c1, c2 in itertools.product(pa1_catg, pa2_catg):
        print(c1, c2, ":", len(catg[c1].intersection(catg[c2])))


"""
import csv
with open('studs.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(["username",
                     "LINp", "LINa", "LINc",
                     "PA2p", "PA2a", "PA2c",
                     "DBSp", "DBSa", "DBSc"
                     "SAPp", "SAPa", "SAPc"
                     ])
    for stud, val in x.items():
        if val == "None":
            writer.writerow([stud, "x", "x", "x", "x", "x", "x", "x", "x", "x"])
        else:
            writer.writerow([stud,
                            val['BI-LIN']['study'], val['BI-LIN']['assessment'], val['BI-LIN']['completed'],
                            val['BI-PA2']['study'], val['BI-PA2']['assessment'], val['BI-PA2']['completed'],
                            val['BI-DBS']['study'], val['BI-DBS']['assessment'], val['BI-DBS']['completed'],
                            val['BI-SAP']['study'], val['BI-SAP']['assessment'], val['BI-SAP']['completed']])

print(x)
"""