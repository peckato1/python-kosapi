import kosapi
import sys
import requests
from kosapi.entity.enum import ParallelType


# ---------------------------------------------------------------------------------------------------------------

def str_course(link):
    return link.split("/")[-2]

# ---------------------------------------------------------------------------------------------------------------

sys.argv = ["a", "BI-AG1", "B181"]

if len(sys.argv) < 2 or len(sys.argv) > 3:
    print("Prints number of students in given COURSE and SEMESTER who are not enrolled in any PARALLEL of given COURSE but have parallels from other courses")
    print("{} COURSE SEMESTER [PARALLEL]".format(sys.argv[0].split("/")[-1]))
    print("   PARALLEL -> tutorial|laboratory")
    sys.exit(1)


# ---------------------------------------------------------------------------------------------------------------

COURSE = sys.argv[1]
SEMESTER = sys.argv[2]
try:
    PARALLEL_TYPE = sys.argv[3]
except IndexError:
    PARALLEL_TYPE = "tutorial"

if PARALLEL_TYPE == "tutorial":
    PARALLEL_TYPE = ParallelType.TUTORIAL
elif PARALLEL_TYPE == "laboratory":
    PARALLEL_TYPE = ParallelType.LABORATORY
else:
    print("Invalid parallel type")
    sys.exit(1)

# ---------------------------------------------------------------------------------------------------------------


def any_true(iterable, func):
    for i in iterable:
        if func(i):
            return True
    return False


# ---------------------------------------------------------------------------------------------------------------

k = kosapi.KosapiClient()

students = 0
enrolled = 0
not_enrolled = 0
not_enrolled_and_other_parallels = 0

not_enrolled_list = set()
not_enrolled_and_other_parallels_list = set()

for stud in k.get().courses(COURSE).students(sem=SEMESTER, limit=500):
    try:
        enrolled_courses = {str_course(course.course.link) for course in k.get().students(stud.username).enrolledCourses(sem=SEMESTER, limit=100)}
        enrolled_parallels = {parallel for parallel in k.get().students(stud.username).parallels(sem=SEMESTER, limit=100)}
    except requests.exceptions.HTTPError:
        continue

    print("Checking: {}".format(stud.username), file=sys.stderr)

    students += 1
    has_parallel_course = False
    if any_true(enrolled_parallels, lambda p: str_course(p.course.link) == COURSE and p.parallelType == PARALLEL_TYPE):
        has_parallel_course = True

    if has_parallel_course:
        enrolled += 1
    else:
        not_enrolled += 1
        not_enrolled_list.add(stud.username)

    if not has_parallel_course and any_true(enrolled_parallels, lambda p: str_course(p.course.link) != COURSE):
        not_enrolled_and_other_parallels += 1
        not_enrolled_and_other_parallels_list.add(stud.username)

print("Course {}: students={}, enrolled_in_tutorial={}, not_enrolled={}, not_enrolled_and_other_parallels={})".format(COURSE, students, enrolled, not_enrolled, not_enrolled_and_other_parallels))
print("not_enrolled:", not_enrolled_list)
print("not_enroLled_and_other_parallems:", not_enrolled_and_other_parallels_list)