import sys
import kosapi


CLIENT_ID = None
CLIENT_SECRET = None


if __name__ == '__main__':
    if len(sys.argv) <= 2:
        print("Usage: %s SEMESTER_CODE COURSE_CODE" % sys.argv[0])
        sys.exit(1)

    sem = sys.argv[1]
    code = sys.argv[2]

    k = kosapi.KosapiClient()

    #for p in k.resource("https://kosapi.fit.cvut.cz/api/3b/courses/%s/parallels?sem=%s" % (code, sem)):
    for p in k.get().courses(code).parallels(sem=sem):
        if p.parallelType == kosapi.entity.enum.ParallelType.LABORATORY:
            print(p.parallelType,
                  p.code,
                  p.timetableSlot.get_day(), p.timetableSlot.get_duration(),
                  p.timetableSlot.room,
                  p.enrollment, p.occupied, p.capacity,
                  p.teacher,
                  p.updated)
