import kosapi
import sys


CLIENT_ID=""
CLIENT_SECRET=""


k = kosapi.KosapiClient(CLIENT_ID, CLIENT_SECRET)

def opakovaci(course_code, sem_code):
    ret = dict()

    for stud in k.resource("https://kosapi.fit.cvut.cz/api/3b/courses/%s/students?limit=500&sem=%s" % (course_code, sem_code)):
        for ppl in k.resource("https://kosapi.fit.cvut.cz/api/3b/people/%s" % stud.username):
            for stud_role in ppl.roles:
                if 'teacher' not in stud_role.link: # TEMPORARY; can't differentiate between roles yet (alternative: isinstance)
                    for c_enroll in k.resource("https://kosapi.fit.cvut.cz/api/3b/%s/enrolledCourses?sem=none" % (stud_role.link)):
                        if c_enroll.course is None or c_enroll.semester is None: # TEMPORARY; differentiate between internal and external course enrollment
                            continue

                        if course_code in str(c_enroll.course.link) and sem_code not in str(c_enroll.semester.link): # TEMPORARY; can't follow xlinks yet
                            try:
                                ret[stud].append(c_enroll.semester)
                            except KeyError:
                                ret[stud] = [c_enroll.semester]
    return ret


def pocet_stud(course_code, sem):
    for c in k.resource("https://kosapi.fit.cvut.cz/api/3b/courses/%s/?sem=%s" % (course_code, sem)):
        return c.instance.occupied


if __name__ == '__main__':
    if len(sys.argv) <= 2:
        print("Usage: %s SEMESTER_CODE COURSE_CODE_1 [COURSE_CODE_i ...]" % sys.argv[0])
        sys.exit(1)

    sem = sys.argv[1]
    for code in sys.argv[2:]:
        print("Predmet: %s v semestru %s" % (code, sem))
        ret = opakovaci(code, sem)

        stud = pocet_stud(code, sem)
        print("%s: Opakovacu: %d, Studentu: %d, pomer=%f %%" % (code, len(ret), stud, len(ret)/stud * 100))

        for key, val in ret.items():
            print("\t", key.firstName, key.lastName, key.username, val)