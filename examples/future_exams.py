import sys
import kosapi
import datetime
import pprint


CLIENT_ID = None
CLIENT_SECRET = None


if __name__ == '__main__':
    if len(sys.argv) <= 2:
        print("Usage: %s SEMESTER_CODE COURSE_CODE" % sys.argv[0])
        sys.exit(1)

    sem = sys.argv[1]
    code = sys.argv[2]

    k = kosapi.KosapiClient(CLIENT_ID, CLIENT_SECRET)

    #for i in k.resource("https://kosapi.fit.cvut.cz/api/3b/courses/%s/exams?sem=%s" % (code, sem)):
    for i in k.get().courses(code).exams(sem=sem):
        if i.startDate >= datetime.datetime.now():
            pprint.pprint(i.fields())
