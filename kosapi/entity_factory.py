from kosapi.utils.class_loader import DynamicClassLoader


class EntityFactory:

    MODULE_MAP = {
        "Branch": "kosapi.entity.branch",
        "Course": "kosapi.entity.course",
        "Instance": "kosapi.entity.course",
        "CourseEvent": "kosapi.entity.course_event",
        "CourseEnrollment": "kosapi.entity.course_enrollment",
        "ExternalCourseEnrollment": "kosapi.entity.course_enrollment",
        "CoursesGroup": "kosapi.entity.courses_group",
        "Coursin": "kosapi.entity.coursin",
        "Division": "kosapi.entity.division",
        "DSpaceFile": "kosapi.entity.dspace_file",
        "Exam": "kosapi.entity.exam",
        "Parallel": "kosapi.entity.parallel",
        "TimetableSlot": "kosapi.entity.parallel",
        "Parameter": "kosapi.entity.parameter",
        "Pathway": "kosapi.entity.pathway",
        "Person": "kosapi.entity.person",
        "Programme": "kosapi.entity.programme",
        "Room": "kosapi.entity.room",
        "Semester": "kosapi.entity.semester",
        "StateExam": "kosapi.entity.state_exam",
        "Student": "kosapi.entity.student",
        "StudyPlan": "kosapi.entity.study_plan",
        "Teacher": "kosapi.entity.teacher",
        "Thesis": "kosapi.entity.thesis",
        "ThesisDraft": "kosapi.entity.thesis_draft",
        "ThesisReview": "kosapi.entity.thesis_review",
    }

    XSI_MAP = {
        "branch": "Branch",
        "course": "Course",
        "courseEvent": "CourseEvent",
        "coursesGroup": "CoursesGroup",
        "coursin": "Coursin",
        "division": "Division",
        "dSpaceFile": "DSpaceFile",
        "exam": "Exam",
        "internalCourseEnrollment": "CourseEnrollment",
        "externalCourseEnrollment": "ExternalCourseEnrollment",
        "parallel": "Parallel",
        "kosetting": "Parameter",
        "pathway": "Pathway",
        "person": "Person",
        "programme": "Programme",
        "room": "Room",
        "semester": "Semester",
        "stateExam": "StateExam",
        "student": "Student",
        "studyPlan": "StudyPlan",
        "teacher": "Teacher",
        "thesis": "Thesis",
        "thesisDraft": "ThesisDraft",
        "thesisReview": "ThesisReview",
    }

    @classmethod
    def get_class(cls, name):
        return DynamicClassLoader.load(cls.MODULE_MAP[name], name)

    @classmethod
    def create_instance(cls, cls_name, **values):
        if type(cls_name) is str:
            entity_cls = cls.get_class(cls_name)
        else:
            entity_cls = cls_name
        return entity_cls(**values)

    @classmethod
    def create_instance_xsi(cls, xsi_name, **values):
        entity_cls = cls.get_class(cls.XSI_MAP[xsi_name])
        return entity_cls(**values)

