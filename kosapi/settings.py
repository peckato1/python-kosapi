import configparser
import os

CONFIG_FILES = [
    '/etc/default/kosapi',
    os.path.expanduser('~/.config/kosapi/kosapi.conf'),
    './kosapi.conf'
]

API_URLS = {
    'base_url': "https://kosapi.fit.cvut.cz/api/3b/",
    'refresh_url': "https://auth.fit.cvut.cz/oauth/token",
    'token_url': "https://auth.fit.cvut.cz/oauth/token",
}

XML_NAMESPACES = {
    'kosapi': "http://kosapi.feld.cvut.cz/schema/3",
    'atom': "http://www.w3.org/2005/Atom",
    'xsi': "http://www.w3.org/2001/XMLSchema-instance",
    'xlink': "http://www.w3.org/1999/xlink",
    'xml': "http://www.w3.org/XML/1998/namespace",
}


class ClientSettings:
    def __init__(self):
        self.settings = configparser.ConfigParser(allow_no_value=True)
        self._load_client_default()
        self.settings.read(CONFIG_FILES)

    def _load_client_default(self):
        self.settings['kosapi'] = {
            'client_id': None,
            'client_secret': None,
            'language_code': 'cs',
            'faculty_code': 'f8',
        }


client_settings = ClientSettings().settings['kosapi']
