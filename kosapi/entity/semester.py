from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Semester(BaseEntity):
    endDate = fields.DateField()
    name = fields.MultiLangStringField()
    startDate = fields.DateField()