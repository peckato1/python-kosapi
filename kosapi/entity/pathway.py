from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Pathway(BaseEntity):
    name = fields.MultiLangStringField()
    note = fields.StringField()
    studyPlan = fields.XLinkField("StudyPlan")