from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class StateExam(BaseEntity):
    branch = fields.XLinkField("Branch")
    endDate = fields.DateField()
    note = fields.StringField()
    programmeType = fields.EnumField(enum.ProgrammeType)
    signinDeadline = fields.DateField()
    startDate = fields.DateField()