from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Branch(BaseEntity):
    abbrev = fields.StringField()
    capacity = fields.IntegerField()
    code = fields.StringField()
    description = fields.MultiLangStringField()
    diplomaName = fields.MultiLangStringField()
    division = fields.XLinkField("Division")
    guarantor = fields.XLinkField("Person")
    name = fields.MultiLangStringField()
    openForAdmission = fields.BooleanField()