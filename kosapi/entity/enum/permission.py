from . import BaseEnum


class Permission(BaseEnum):
    ALLOWED = 1
    DENIED = 2
    UNDEFINED = 3