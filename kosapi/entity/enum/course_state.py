from . import BaseEnum


class CourseState(BaseEnum):
    PROPOSED = 1
    APPROVED = 2
    OPEN = 3
    CLOSED = 4
    UNDEFINED = 5