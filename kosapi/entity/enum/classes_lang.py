from . import BaseEnum


class ClassesLang(BaseEnum):
    CS = 1
    DE = 2
    EN = 3
    ES = 4
    FR = 5
    PL = 6
    RU = 7
    SK = 8
    UNDEFINED = 9
