from . import BaseEnum


class StudyState(BaseEnum):
    ACTIVE = 1
    INTERRUPTED = 2
    CLOSED = 3
    UNDEFINED = 4