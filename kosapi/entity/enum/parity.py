from . import BaseEnum


class Parity(BaseEnum):
    ODD = 1
    EVEN = 2
    BOTH = 3
    UNDEFINED = 4