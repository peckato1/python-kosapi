from . import BaseEnum


class TermType(BaseEnum):
    ASSESSMENT = 1
    FINAL_EXAM = 2