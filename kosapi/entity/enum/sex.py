from . import BaseEnum


class Sex(BaseEnum):
    MALE = 1
    FEMALE = 2
    UNDEFINED = 3