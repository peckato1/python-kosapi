from . import BaseEnum


class DivisionType(BaseEnum):
    RECTORATE = 1
    FACULTY = 2
    DEPARTMENT = 3
    INSTITUTE = 4
    UNDEFINED = 5