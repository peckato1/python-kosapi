from . import BaseEnum


class ProgrammeType(BaseEnum):
    BACHELOR = 1
    DOCTORAL = 2
    INTERNSHIP = 3
    LIFELONG = 4
    MASTER = 5
    MASTER_LEGACY = 6
    UNDEFINED = 7