from . import BaseEnum


class Season(BaseEnum):
    WINTER = 1
    SUMMER = 2
    BOTH = 3
    UNDEFINED = 4