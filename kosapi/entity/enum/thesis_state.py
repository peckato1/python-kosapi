from . import BaseEnum


class ThesisState(BaseEnum):
    AVAILABLE = 1
    RESERVED = 2
    ASSIGNED = 3
    SUBMITTED = 4
    ACCEPTED = 5
    DEFENDED = 6
    UNDEFINED = 7
