from . import BaseEnum


class ParallelType(BaseEnum):
    LABORATORY = 1
    LECTURE = 2
    TUTORIAL = 3
    UNDEFINED = 4