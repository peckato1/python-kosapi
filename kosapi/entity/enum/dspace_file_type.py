from . import BaseEnum


class DSpaceFileType(BaseEnum):
    ATTACHMENT = 1
    REVIEW = 2
    THESIS = 3