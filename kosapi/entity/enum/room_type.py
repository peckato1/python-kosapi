from . import BaseEnum


class RoomType(BaseEnum):
    LABORATORY = 1
    LECTURE = 2
    OFFICE = 3
    TUTORIAL = 4
    UNDEFINED = 5