from . import BaseEnum


class StudyForm(BaseEnum):
    FULLTIME = 1
    PARTTIME = 2
    DISTANCE = 3
    SELF_PAYER = 4
    Z = 5 # Prijezd kratkodoby
    W = 6 # Vyjezd kratkodoby - kombinovany
    V = 7 # Vyjezd kratkodoby
    UNDEFINED = 8