from . import BaseEnum


class ClassesType(BaseEnum):
    ATELIER = 1
    BLOCK = 2
    CONSULTATION = 3
    LABORATORY = 4
    LECTURE = 5
    PROJECT = 6
    PROJECT_INDV = 7
    PROJECT_TEAM = 8
    PROSEMINAR = 9
    PT_COURSE = 10
    SEMINAR = 11
    TUTORIAL = 12
    UNDEFINED = 13
