from . import BaseEnum


class TeacherRole(BaseEnum):
    EDITOR = 1
    EXAMINER = 2
    GUARANTOR = 3
    INSTRUCTOR = 4
    LECTURER = 5
    UNDEFINED = 6