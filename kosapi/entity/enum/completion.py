from . import BaseEnum


class Completion(BaseEnum):
    CLFD_CREDIT = 1
    CREDIT = 2
    CREDIT_EXAM = 3
    DEFENCE = 4
    EXAM = 5
    NOTHING = 6
    UNDEFINED = 7