from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Room(BaseEntity):
    capacity = fields.DictField(fields.IntegerField(), attr="for")
    code = fields.StringField()
    locality = fields.StringField()
    name = fields.MultiLangStringField()
    division = fields.XLinkField("Division")
    type = fields.EnumField(enum.RoomType)