from . import fields


class BaseEntity(metaclass=fields.DescriptorAutoNameMeta):
    id = fields.IdField()
    updated = fields.UpdatedField()
    title = fields.TitleField()
    link = fields.LinkField()
    author = fields.AuthorField()

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            self[k] = v

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, val):
        return setattr(self, key, val)

    def field_descriptor(self, field_name):
        return getattr(type(self), field_name)

    def fields(self):
        return {attr: self[attr] for attr in type(self).__dict__ if
                isinstance(self.field_descriptor(attr), fields.BaseField)}

    def items(self):
        return self.fields().items()
