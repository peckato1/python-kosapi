from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Parameter(BaseEntity):
    description = fields.StringField()
    key = fields.StringField()
    value = fields.StringField()