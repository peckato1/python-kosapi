from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Person(BaseEntity):
    firstName = fields.StringField()
    lastName = fields.StringField()
    personalNumber = fields.StringField()
    roles = fields.XLinkListField(fields.XLinkField(["Student"]), nested=True) # TODO i ucitele!!!
    titlesPost = fields.StringField()
    titlesPre = fields.StringField()
    username = fields.StringField()