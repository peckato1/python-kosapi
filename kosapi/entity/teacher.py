from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Teacher(BaseEntity):
    division = fields.XLinkField("Division")
    email = fields.StringField()
    extern = fields.BooleanField()
    firstName = fields.StringField()
    lastName = fields.StringField()
    personalNumber = fields.StringField()
    phone = fields.StringField()
    stageName = fields.StringField()
    supervisionPhDStudents = fields.EnumField(enum.Permission)
    titlesPost = fields.StringField()
    titlesPre = fields.StringField()
    username = fields.StringField()
