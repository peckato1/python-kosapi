from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum
from kosapi.utils.timetable import TimetableUtils
from kosapi.settings import client_settings


class TimetableSlot(BaseEntity):
    day = fields.IntegerField()
    duration = fields.IntegerField()
    firstHour = fields.IntegerField()
    parity = fields.EnumField(enum.Parity)
    room = fields.XLinkField("Room")
    startTime = fields.StringField()
    endTime = fields.StringField()
    teacher = fields.XLinkListField(fields.XLinkField("Teacher"), nested=False)
    weeks = fields.StringField()

    def get_day(self, lang=client_settings['language_code']):
        return TimetableUtils.day_to_string(self.day, lang)

    def get_hour(self, faculty=client_settings['faculty_code']):
        return TimetableUtils.hour_to_time_string(self.firstHour, faculty)

    def get_duration(self, faculty=client_settings['faculty_code']):
        return TimetableUtils.duration(self.firstHour, self.duration, faculty)


class Parallel(BaseEntity):
    capacity = fields.IntegerField()
    capacityOverfill = fields.EnumField(enum.Permission)
    code = fields.IntegerField()
    course = fields.XLinkField("Course")
    parallelType = fields.EnumField(enum.ParallelType)
    enrollment = fields.EnumField(enum.Permission)
    note = fields.MultiLangStringField()
    occupied = fields.IntegerField()
    semester = fields.XLinkField("Semester")
    teacher = fields.XLinkListField(fields.XLinkField("Teacher"), nested=False)
    timetableSlot = fields.EntityField("TimetableSlot")
