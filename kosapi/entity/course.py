from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Instance(BaseEntity):
    semester = fields.XLinkField("Semester")
    capacity = fields.DictField(fields.IntegerField(), attr='of')
    capacityOverfill = fields.EnumField(enum.Permission)
    occupied = fields.IntegerField()
    examiners = fields.XLinkListField(fields.XLinkField("Teacher"))
    guarantors = fields.XLinkListField(fields.XLinkField("Teacher"))
    instructors = fields.XLinkListField(fields.XLinkField("Teacher"))
    lecturers = fields.XLinkListField(fields.XLinkField("Teacher"))
    editors = fields.XLinkListField(fields.XLinkField("Teacher"))


class Course(BaseEntity):
    allowedEnrollmentCount = fields.IntegerField()
    approvalDate = fields.DateField()
    classesLang = fields.EnumField(enum.ClassesLang)
    classesType = fields.EnumField(enum.ClassesType) # TODO list, ale neni v podstromu
    code = fields.StringField()
    completion = fields.EnumField(enum.Completion)
    credits = fields.IntegerField()
    department = fields.XLinkField("Division")
    description = fields.MultiLangStringField()
    homepage = fields.StringField()
    keywords = fields.MultiLangStringField()
    lecturesContents = fields.MultiLangStringField()
    literature = fields.MultiLangStringField()
    name = fields.MultiLangStringField()
    note = fields.MultiLangStringField()
    objectives = fields.MultiLangStringField()
    programmeType = fields.EnumField(enum.ProgrammeType)
    range = fields.StringField()
    requirements = fields.MultiLangStringField()
    season = fields.EnumField(enum.Season)
    state = fields.EnumField(enum.CourseState)
    studyForm = fields.EnumField(enum.StudyForm)
    superiorCourse = fields.XLinkField("Course")
    subcourses = fields.XLinkListField(fields.XLinkField("Course"))
    tutorialsContents = fields.MultiLangStringField()
    instance = fields.EntityField("Instance")
