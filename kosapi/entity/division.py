from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Division(BaseEntity):
    abbrev =fields.MultiLangStringField()
    code = fields.StringField()
    name = fields.MultiLangStringField()
    parent = fields.XLinkField("Division")
    divisionType = fields.EnumField(enum.DivisionType)