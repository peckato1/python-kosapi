from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class ThesisReview(BaseEntity):
    thesis = fields.XLinkField("Thesis")
    author = fields.XLinkField("Teacher")
    proposedGrade = fields.StringField()
    documentUrl = fields.StringField()
