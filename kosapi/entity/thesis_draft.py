from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class ThesisDraft(BaseEntity):
    branch = fields.XLinkField("Branch")
    creator = fields.XLinkField("Person")
    department = fields.XLinkField("Division")
    description = fields.StringField()
    name = fields.StringField()
    type = fields.EnumField(enum.ProgrammeType)