from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class DSpaceFile(BaseEntity):
    author = fields.XLinkField("Teacher")
    type = fields.EnumField(enum.DSpaceFileType)
    url = fields.StringField()