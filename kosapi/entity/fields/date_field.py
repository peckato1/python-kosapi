from .base_field import BaseField

import datetime


class DateField(BaseField):
    def __init__(self):
        super().__init__(type=datetime.date)

    def from_string(self, val):
        return datetime.datetime.strptime(val, '%Y-%m-%d').date()