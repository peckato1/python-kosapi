import datetime

from . import StringField, DateTimeField, XLinkField


class BaseMetaField:
    pass


class AuthorField(BaseMetaField, StringField): # TODO returns username only. Can't we do xlink to people/<username> ?
    pass


class UpdatedField(BaseMetaField, DateTimeField):
    def from_string(self, val):
        return datetime.datetime.strptime(val, '%Y-%m-%dT%H:%M:%S.%f')


class IdField(BaseMetaField, StringField):
    pass


class TitleField(BaseMetaField, StringField):
    pass


class LinkField(BaseMetaField, StringField): # TODO subclass from XLinkField
    pass