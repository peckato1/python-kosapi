from .base_field import BaseField

from kosapi.entity.enum import BaseEnum


class EnumField(BaseField):
    def __init__(self, enum_type):
        self.subclass_check(enum_type, [BaseEnum])
        super().__init__(type=enum_type)

    def from_string(self, val):
        enum_cls = self._type
        return enum_cls[val.text]