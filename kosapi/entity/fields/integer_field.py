from .base_field import BaseField


class IntegerField(BaseField):
    def __init__(self, min_val=None, max_val=None):
        self.type_check(min_val, [int], none=True)
        self.type_check(max_val, [int], none=True)

        super().__init__(type=int)

        self._interval = (min_val, max_val)

    def _constraints_check(self, val):
        lb = self._interval[0] <= val if self._interval[0] is not None else True
        ub = self._interval[1] >= val if self._interval[1] is not None else True
        return lb and ub

    def from_string(self, val):
        return int(val)