from .dict_field import DictField
from .string_field import StringField


def MultiLangStringField(*args, **kwargs):
    return DictField(StringField(*args, **kwargs), attr_ns='xml', attr='lang')