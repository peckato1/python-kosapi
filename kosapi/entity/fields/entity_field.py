from .base_field import BaseField
from kosapi.entity_factory import EntityFactory


class EntityField(BaseField):
    def __init__(self, entity_type):
        if type(entity_type) is str:
            entity_type = EntityFactory.get_class(entity_type)

        super().__init__(type=entity_type)