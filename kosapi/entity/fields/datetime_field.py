from .base_field import BaseField

import datetime


class DateTimeField(BaseField):
    def __init__(self):
        super().__init__(type=datetime.datetime)

    def from_string(self, val):
        return datetime.datetime.strptime(val, '%Y-%m-%dT%H:%M:%S')