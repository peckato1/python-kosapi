from kosapi.utils import visitor


class DescriptorAutoNameMeta(type):
    def __new__(mcs, classname, bases, classDict):
        for name, attr in classDict.items():
            if isinstance(attr, BaseField):
                attr._name = "_field_%s" % name
                attr._unmangled_name = name
        return type.__new__(mcs, classname, bases, classDict)


class BaseField(visitor.Visitable):
    def __init__(self, type=None):
        self._type = type

    def __get__(self, instance, objtype=None):
        if instance is None:
            return self

        return self._get_value(instance)

    def __set__(self, instance, value):
        if not self._type_check(value):
            raise TypeError("Can't assign to field (Required type: '%s', value type: '%s')" % (self._type, type(value)))

        if not self._constraints_check(value):
            raise ValueError("Can't assign to field (Constraints check failed for field type '%s')" % self.__class__.__name__)

        self._set_value(instance, value)

    def __delete__(self, instance):
        raise AttributeError("Can't delete attribute")

    def _type_check(self, val):
        return self._type is type(val)

    def _constraints_check(self, val):
        return True

    def _set_value(self, instance, val):
        instance.__dict__[self._name] = val

    def _get_value(self, instance):
        return instance.__dict__.get(self._name, None)

    def accept(self, visitor_obj, arg):
        return visitor_obj.visit(self, arg)

    @staticmethod
    def type_check(val, allowed_types=None, none=False):
        if allowed_types is None:
            allowed_types = []

        if none is True and val is None:
            return

        if type(val) not in allowed_types:
            raise TypeError("Invalid type passed as argument to field constructor")

    @staticmethod
    def subclass_check(cls, allowed_superclasses=None):
        if allowed_superclasses is None:
            allowed_superclasses = []

        for superclass in allowed_superclasses:
            if issubclass(cls, superclass):
                return

        raise TypeError("Enum type in EnumField must be subclass of BaseEnum")

    def from_string(self, val):
        raise NotImplementedError()