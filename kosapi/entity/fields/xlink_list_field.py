from .base_field import BaseField
from kosapi.entity_factory import EntityFactory


class XLinkListField(BaseField):
    def __init__(self, xlink_field, nested=True, nested_tag=None):
        super().__init__(type=list)
        self._xlink_field = xlink_field
        self._nested = nested

    def _constraints_check(self, val):
        if not super()._constraints_check(val):
            return False

        #return all(self._xlink_field._unmangled_name == xproxy._tag for xproxy in val)
        return True

    def _set_value(self, instance, val, tag=None):
        if self._nested is True:
            return super()._set_value(instance, val)
        else:
            if instance.__dict__.get(self._name, None) is None:
                instance.__dict__[self._name] = val
            else:
                instance.__dict__[self._name].extend(val)