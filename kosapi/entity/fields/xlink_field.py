from .base_field import BaseField


class XLinkFieldProxy:
    def __init__(self, tag, value, link):
        self._tag = tag
        self._value = value
        self._link = link
    """
        self._entity = None
    """

    def __get__(self):
        return self._value

    def __repr__(self):
        return self._value if self._value is not None else "None"

    """
    def __getattr__(self, attr):
        if self._entity is None:
            self._entity = EntityLoader
        return getattr(self._entity, attr)

    def __setattr__(self, key, value):
        if self._entity is None:
            self._entity =
        return setattr(self._entity, key, value)
    """

    @property
    def link(self):
        return self._link


class XLinkField(BaseField):
    def __init__(self, entity_type_str):
        super().__init__(type=XLinkFieldProxy)
        self._link_type = entity_type_str
        self._entity_type_str = entity_type_str

    def _constraints_check(self, val):
        return self._unmangled_name == val._tag