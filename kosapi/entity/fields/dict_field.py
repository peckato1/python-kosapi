from .base_field import BaseField


class DictField(BaseField):
    def __init__(self, field, attr, attr_ns=None):
        super().__init__(type=tuple)
        self._field = field
        self._attr = (attr_ns, attr)

    def _set_value(self, instance, val):
        """ Update dict, not overwrite """

        try:
            valdict = getattr(instance, self._name)
        except AttributeError:
            setattr(instance, self._name, dict())
            valdict = getattr(instance, self._name)

        valdict[val[0]] = val[1]

    def _get_value(self, instance):
        return instance.__dict__.get(self._name, None)

    def _type_check(self, val):
        return isinstance(val[0], str) and self._field._type_check(val[1])

    def _constraints_check(self, val):
        return self._field._constraints_check(val)
