from .base_field import BaseField


class BooleanField(BaseField):
    def __init__(self):
        super().__init__(type=bool)

    def from_string(self, val):
        if val.lower() == "true":
            return True
        elif val.lower() == "false":
            return False
        else:
            raise ValueError