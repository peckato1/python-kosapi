from .base_field import BaseField


class StringField(BaseField):
    def __init__(self, max_len=None):
        self.type_check(max_len, [int], none=True)

        super().__init__(type=str)

        if max_len is not None and max_len < 0:
            raise ValueError

        self._max_len = max_len

    def _constraints_check(self, val):
        if self._max_len is not None:
            return len(val) <= self._max_len
        return True

    def from_string(self, val):
        return val