from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class StudyPlan(BaseEntity):
    approvalDate = fields.DateField()
    approved = fields.BooleanField()
    branch = fields.XLinkField("Branch")
    code = fields.StringField()
    creditsMinLimit = fields.IntegerField()
    division = fields.XLinkField("Division")
    individual = fields.BooleanField()
    name = fields.MultiLangStringField()
    note = fields.StringField()
    programme = fields.XLinkField("Programme")
    studyForm = fields.EnumField(enum.StudyForm)