from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Thesis(BaseEntity):
    assignmentDate = fields.DateField()
    branch = fields.XLinkField("Branch")
    department = fields.XLinkField("Division")
    defenseDate = fields.DateField()
    description = fields.StringField()
    keywords = fields.MultiLangStringField()
    lang = fields.EnumField(enum.ClassesLang)
    licenceAgreed = fields.BooleanField()
    literature = fields.StringField()
    name = fields.MultiLangStringField()
    reviewer = fields.XLinkField("Person")
    proponent = fields.StringField()
    state = fields.EnumField(enum.ThesisState)
    student = fields.XLinkField("Student")
    submissionDeadline = fields.DateField()
    supervisor = fields.XLinkField("Person")
    summary = fields.MultiLangStringField()
    type = fields.EnumField(enum.ProgrammeType)
    dspaceUrl = fields.StringField()