from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class CoursesGroup(BaseEntity):
    approved = fields.BooleanField()
    code = fields.StringField()
    coursesMaxLimit = fields.IntegerField()
    coursesMinLimit = fields.IntegerField()
    creditsMaxLimit = fields.IntegerField()
    creditsMinLimit = fields.IntegerField()
    name = fields.MultiLangStringField()
    note = fields.MultiLangStringField()
    role = fields.StringField()
    courses = fields.XLinkListField(fields.XLinkField("Course"))