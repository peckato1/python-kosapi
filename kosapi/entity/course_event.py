from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class CourseEvent(BaseEntity):
    targetBranch = fields.XLinkField("Branch") # list ale neni v podstromu
    capacity = fields.IntegerField()
    course = fields.XLinkField("Course")
    creator = fields.XLinkField("Teacher")
    endDate = fields.DateTimeField()
    name = fields.MultiLangStringField()
    note = fields.MultiLangStringField()
    occupied = fields.IntegerField()
    room = fields.XLinkField("Room")
    semester = fields.XLinkField("Semester")
    signinDeadline =  fields.DateField()
    startDate = fields.DateTimeField()