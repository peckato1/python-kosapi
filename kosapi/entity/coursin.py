from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Coursin(BaseEntity):
    capacity = fields.IntegerField()
    capacityOverfill = fields.EnumField(enum.Permission)
    course = fields.XLinkField("Course")
    occupied = fields.IntegerField()
    semester = fields.XLinkField("Semester")
    tutorialCapacity = fields.IntegerField()
    examiners = fields.XLinkListField(fields.XLinkField("Teacher"))
    guarantors = fields.XLinkListField(fields.XLinkField("Teacher"))
    instructors = fields.XLinkListField(fields.XLinkField("Teacher"))
    lecturers = fields.XLinkListField(fields.XLinkField("Teacher"))
    editors = fields.XLinkListField(fields.XLinkField("Teacher"))
