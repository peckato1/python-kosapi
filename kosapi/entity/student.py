from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Student(BaseEntity):
    branch = fields.XLinkField("Branch")
    code = fields.StringField() # TODO what is this for?
    department = fields.XLinkField("Division")
    email = fields.StringField()
    startDate = fields.DateField()
    faculty = fields.XLinkField("Division")
    firstName = fields.StringField()
    grade = fields.IntegerField()
    interruptedUntil = fields.DateField()
    lastName = fields.StringField()
    personalNumber = fields.StringField()
    programme = fields.XLinkField("Programme")
    endDate = fields.DateField()
    studyForm = fields.EnumField(enum.StudyForm)
    studyGroup = fields.IntegerField()
    studyPlan = fields.XLinkField("StudyPlan")
    studyState = fields.EnumField(enum.StudyState)
    supervisor = fields.XLinkField("Teacher")
    supervisorSpecialist = fields.XLinkField("Teacher")
    studyTerminationReason = fields.EnumField(enum.StudyTermination)
    titlesPost = fields.StringField()
    titlesPre = fields.StringField()
    username = fields.StringField()