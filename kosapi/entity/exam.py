from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Exam(BaseEntity):
    cancelDeadline = fields.DateField()
    capacity = fields.IntegerField()
    course = fields.XLinkField("Course")
    creditRequired = fields.BooleanField()
    department = fields.XLinkField("Division")
    endDate = fields.DateTimeField()
    examiner = fields.XLinkField("Teacher")
    examiners = fields.XLinkListField(fields.XLinkField("Teacher"))
    note = fields.StringField()
    occupied = fields.IntegerField()
    resit = fields.BooleanField()
    room = fields.XLinkField("Room")
    semester = fields.XLinkField("Semester")
    signinDeadline = fields.DateField()
    startDate = fields.DateTimeField()
    substitutes = fields.EnumField(enum.Permission)
    superior = fields.XLinkField("Exam")
    termType = fields.EnumField(enum.TermType)