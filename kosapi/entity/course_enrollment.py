from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class CourseEnrollment(BaseEntity):
    assessedDate = fields.DateField()
    assessment = fields.BooleanField()
    completed = fields.BooleanField()
    extern = fields.BooleanField()
    gradedDate = fields.DateField()
    semester = fields.XLinkField("Semester")
    course = fields.XLinkField("Course")


class ExternalCourseEnrollment(CourseEnrollment):
    completion = fields.EnumField(enum.Completion)
    credits = fields.IntegerField()
    code = fields.StringField()
    name = fields.StringField()