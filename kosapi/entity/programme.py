from .base_entity import BaseEntity

from kosapi.entity import fields
from kosapi.entity import enum


class Programme(BaseEntity):
    academicTitle = fields.StringField()
    capacity = fields.IntegerField()
    classesLang = fields.EnumField(enum.ClassesLang)
    code = fields.StringField()
    description = fields.MultiLangStringField()
    diplomaName = fields.MultiLangStringField()
    faculty = fields.XLinkField("Division")
    guarantor = fields.XLinkField("Teacher")
    name = fields.MultiLangStringField()
    openForAdmission = fields.BooleanField()
    studyDuration = fields.IntegerField()
    type = fields.EnumField(enum.ProgrammeType)