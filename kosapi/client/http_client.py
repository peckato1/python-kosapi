from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session
from requests.compat import urljoin

from kosapi import settings


class HttpClient:
    def __init__(self, client_id=None, client_secret=None):
        self._token = None
        self._auth_dict = None
        self._session = None

        if client_id is None:
            client_id = settings.client_settings['client_id']
        if client_secret is None:
            client_secret = settings.client_settings['client_secret']

        self._auth_dict = {'client_id': client_id,
                           'client_secret': client_secret}

    def init_session(self):
        def token_saver(token):
            self._token = token

        self._session = OAuth2Session(
            client=BackendApplicationClient(client_id=self._auth_dict['client_id']),
            auto_refresh_kwargs=self._auth_dict,
            auto_refresh_url=settings.API_URLS['refresh_url'],
            token_updater=token_saver)

        resp = self._session.fetch_token(token_url=settings.API_URLS['token_url'],
                                        client_id=self._auth_dict['client_id'],
                                        client_secret=self._auth_dict['client_secret'])
        token_saver(resp['access_token'])

    def get_url(self, url, **params):

        if self._session is None:
            self.init_session()

        self._session.headers.update({'Accept': 'application/xml'})

        # TODO check token expired
        resp = self._session.get(url, params=params)
        return resp

    def get_relative(self, url, **params):
        url = self._build_url(url)
        return self.get_url(url, **params)

    @staticmethod
    def _build_url(url):
        return urljoin(settings.API_URLS['base_url'], url)