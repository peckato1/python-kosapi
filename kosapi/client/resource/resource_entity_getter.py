from kosapi.parser import XMLParser


class ResourceEntityGetter:
    def __init__(self, client, resource_url, **kwargs):
        self._client = client
        self._first_url = resource_url
        self._current_url = self._first_url
        self._params = kwargs

        self._first = True

    def get_next(self):
        if self._first is False and self._current_url is None:
            return (False, [], )

        if self._first is True:
            self._first = False
            resp = self._client.get_relative(self._current_url, **self._params)
        else:
            resp = self._client.get_relative(self._current_url)

        resp.raise_for_status()

        parser = XMLParser(resp.text)
        parsed = parser.parse()
        # print("Parsed: %d entity from %s (%s)" % (len(parsed.entities), self._current_url, self._params))

        self._current_url = parsed.next_url
        return (True, parsed.entities, )
