from kosapi.utils.trie import Trie, TrieNode


class ResourceNode(TrieNode):
    pass


class ResourceDefinitions(Trie):
    def add(self, path, *, type=None, feed=False):
        if path[0] != "/":
            return
        path = path[1:]
        data = path.split('/')
        return super().add(data, type=type, feed=feed)

    def _str_node(self, it):
        return str(it.data)

# ----------------------------------------------------------------------------------------------------------------------


resources = ResourceDefinitions()
resources.add("/branches", type="Branch", feed=True)
resources.add("/branches/{id}", type="Branch")
resources.add("/branches/{id}/studyPlans", type="StudyPlan", feed=True)

resources.add("/courseEvents", type="CourseEvent", feed=True)
resources.add("/courseEvents/{id}", type="CourseEvent")
resources.add("/courseEvents/{id}/attendees", type="Student", feed=True)

resources.add("/courses", type="Course", feed=True)
resources.add("/courses/{code}", type="Course")
resources.add("/courses/{code}/events", type="CourseEvent", feed=True)
resources.add("/courses/{code}/exams", type="Exam", feed=True)
resources.add("/courses/{code}/parallels", type="Parallel", feed=True)
resources.add("/courses/{code}/students", type="Student", feed=True)
resources.add("/courses/{code}/instances", type="Coursin", feed=True)
resources.add("/courses/{code}/branches", type="Branch", feed=True)

resources.add("/coursesGroups", type="CoursesGroup", feed=True)
resources.add("/coursesGroups/{code}", type="CoursesGroup")

resources.add("/divisions", type="Division", feed=True)
resources.add("/divisions/{code}", type="Division")
resources.add("/divisions/{code}/courses", type="Course", feed=True)
resources.add("/divisions/{code}/subdivisions", type="Division", feed=True)
resources.add("/divisions/{code}/teachers", type="Teacher", feed=True)

resources.add("/exams", type="Exam", feed=True)
resources.add("/exams/{id}", type="Exam")
resources.add("/exams/{id}/attendees", type="Student", feed=True)

resources.add("/parallels", type="Parallel", feed=True)
resources.add("/parallels/{id}", type="Parallel")
resources.add("/parallels/{id}/related", type="Parallel", feed=True)
resources.add("/parallels/{id}/students", type="Student", feed=True)

resources.add("/parameters", type="Parameter", feed=True)
resources.add("/parameters/{key}", type="Parameter")

resources.add("/pathways", type="Pathway", feed=True)
resources.add("/pathways/{id}", type="Pathway")

resources.add("/people", type="Person", feed=True)
resources.add("/people/{usernameOrId}", type="Person")

resources.add("/programmes", type="Programme", feed=True)
resources.add("/programmes/{code}", type="Programme")
resources.add("/programmes/{code}/branches", type="Branch", feed=True)
resources.add("/programmes/{code}/courses", type="Course", feed=True)
resources.add("/programmes/{code}/coursesGroups", type="CoursesGroup", feed=True)
resources.add("/programmes/{code}/studyPlans", type="StudyPlan", feed=True)

resources.add("/rooms", type="Room", feed=True)
resources.add("/rooms/{code}", type="Room")

resources.add("/semesters", type="Semester", feed=True)
resources.add("/semesters/{code}", type="Semester")
resources.add("/semesters/current", type="Semester")
resources.add("/semesters/next", type="Semester")
resources.add("/semesters/prev", type="Semester")
resources.add("/semesters/scheduling", type="Semester")

resources.add("/stateExams", type="StateExam", feed=True)
resources.add("/stateExams/{id}", type="StateExam")

resources.add("/students", type="Student", feed=True)
resources.add("/students/{studyCodeOrId}", type="Student")
resources.add("/students/{studyCodeOrId}/enrolledCourses", type="CourseEnrollment", feed=True)
resources.add("/students/{studyCodeOrId}/parallels", type="Parallel", feed=True)
resources.add("/students/{studyCodeOrId}/registeredExams", feed=True)
resources.add("/students/{studyCodeOrId}/exams", type="Exam", feed=True)

resources.add("/studyPlans", type="StudyPlan", feed=True)
resources.add("/studyPlans/{code}", type="StudyPlan")
resources.add("/studyPlans/{code}/coursesGroups", type="CoursesGroup", feed=True)
resources.add("/studyPlans/{code}/pathways", type="Pathway", feed=True)

resources.add("/teachers", type="Teacher", feed=True)
resources.add("/teachers/{usernameOrId}", type="Teacher")
resources.add("/teachers/{usernameOrId}/courses", type="Course", feed=True)
resources.add("/teachers/{usernameOrId}/parallels", type="Parallel", feed=True)
resources.add("/teachers/{usernameOrId}/exams", type="Exam", feed=True)
resources.add("/teachers/{usernameOrId}/timetable", type="TeacherTimetableSlot", feed=True)

resources.add("/theses", type="Thesis", feed=True)
resources.add("/theses/{id}", type="Thesis")
resources.add("/theses/{id}/reviews", type="ThesisReview", feed=True)
resources.add("/theses/{id}/files", type="DSpaceFile", feed=True)
resources.add("/theses/{id}/files/main")

resources.add("/thesesDrafts", type="ThesisDraft", feed=True)
resources.add("/thesesDrafts/{id}", type="ThesisDraft")
resources.add("/thesesDrafts/{id}/applicants", type="Student", feed=True)

resources.add("/thesesReviews", type="ThesisReview", feed=True)
resources.add("/thesesReviews/{id}", type="ThesisReview")
resources.add("/thesesReviews/{id}/file")


if __name__ == '__main__':
    resources.print()