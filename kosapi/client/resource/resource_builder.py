from .result_iterator import ResultIterator
from kosapi.errors import KosapiError


class ResourceBuilder:
    def __init__(self, client):
        self.querystring = dict()
        self.items = []
        self._http_client = client

    def __getattr__(self, name):
        def arg_wrap(*args, **kwargs):
            return self.resource(name, *args, **kwargs)
        return arg_wrap

    def resource(self, name, *args, **kwargs):
        for k, v in kwargs.items():
            self.querystring[k] = v

        self.items.append(name)
        for arg in args:
            self.items.append(arg)

        return self

    def _build_url(self):
        return "/".join(self.items)

    def __iter__(self):
        return ResultIterator(self._http_client, self._build_url(), **self.querystring)