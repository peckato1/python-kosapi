from .resource_entity_getter import ResourceEntityGetter


class ResultIterator:
    def __init__(self, client, resource_url, **kwargs):
        self._resource_getter = ResourceEntityGetter(client, resource_url, **kwargs)
        self._buffer = list()

    def __next__(self):
        if len(self._buffer) == 0:
            more, entities = self._resource_getter.get_next()
            if more == False or len(entities) == 0:
                raise StopIteration
            else:
                self._buffer = entities

        ret = self._buffer[0]
        self._buffer = self._buffer[1:]
        return ret
