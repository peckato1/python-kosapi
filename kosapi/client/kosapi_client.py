from kosapi.client.resource import ResultIterator, ResourceBuilder
from .http_client import HttpClient


class KosapiClient:
    def __init__(self, client_id=None, client_secret=None):
        self.http_client = HttpClient(client_id, client_secret)

    def resource(self, url, **kwargs):
        for entry in ResultIterator(self.http_client, url, **kwargs):
            yield entry

    def get(self):
        return ResourceBuilder(self.http_client)
