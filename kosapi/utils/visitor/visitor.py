from kosapi.utils.visitor.visitable import Visitable

class Visitor:
    VISITED_CLASS = None

    def visit(self, item, *args):
        if not isinstance(item, Visitable):
            raise TypeError("Visited item is not instance of of Visitable")

        if self.VISITED_CLASS is not None and not issubclass(type(item), self.VISITED_CLASS):
            raise TypeError("Visitor must visit subclasses of '%s'. Visited type: '%s'" % (self.VISITED_CLASS, item.__class__.__name__))

        method_name = 'visit_' + item.__class__.__name__

        try:
            method = getattr(self, method_name)
        except AttributeError:
            raise AttributeError("Visitor: Method '%s' not found in visitor class '%s'" % (method_name, self.__class__.__name__))

        self.before_visit(item, method)
        ret = method(item, *args)
        return self.after_visit(ret)

    def before_visit(self, item, method):
        pass
    
    def after_visit(self, ret):
        return ret