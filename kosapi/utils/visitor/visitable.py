class Visitable:
    def accept(self, visitor_obj, *args):
        return visitor_obj.visit(self, *args)