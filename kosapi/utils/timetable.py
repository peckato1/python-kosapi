import enum
import re
import datetime


class TimetableSlotUtils:
    class Faculty(enum.Enum):
        FSV = 1
        FS = 2
        FEL = 3
        FJFI = 4
        FA = 5
        FD = 6
        FBMI = 7
        FIT = 8

    class FacultyTimes:
        def __init__(self, faculty, starts=None, ends=None):
            self.faculty = faculty

            if starts is None or ends is None:
                raise ValueError("FacultyTimes starts and ends must be specified")

            self.starts = self._to_time(starts)
            self.ends = self._to_time(ends)

        @classmethod
        def _to_time(cls, lst):
            return [datetime.datetime.strptime(i, '%H:%M').time() for i in lst]

    # ------------------------------------------------------------------------------------------------------------------

    def __init__(self):
        self.faculties = dict()

    @classmethod
    def _faculty_to_enum(cls, faculty):
        if isinstance(faculty, str):
            try:
                return cls.Faculty[faculty.upper()]
            except KeyError:
                pass

            m = re.match("^[fF](?P<fid>\d+)$", faculty)
            if m is not None:
                return cls.Faculty(int(m.group('fid')))

        elif isinstance(faculty, int):
            return cls.Faculty(faculty)

        elif isinstance(faculty, cls.Faculty):
            return faculty

    def day_to_string(self, day, lang):
        if lang == "cs":
            return ["Po", "Ut", "St", "Ct", "Pa", "So", "Ne"][day - 1]
        elif lang == "en":
            return ["Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"][day - 1]

    def hour_to_time_string(self, hour, faculty):
        faculty = self._faculty_to_enum(faculty)
        return self.faculties[faculty].starts[hour - 1]

    def duration(self, first_hour, duration, faculty):
        faculty = self._faculty_to_enum(faculty)
        return self.faculties[faculty].starts[first_hour - 1], self.faculties[faculty].ends[first_hour + duration - 1]

    def add_faculty(self, faculty_times):
        self.faculties[faculty_times.faculty] = faculty_times

# -----------------------------------------------------------------------------

TimetableUtils = TimetableSlotUtils()


TimetableUtils.add_faculty(TimetableSlotUtils.FacultyTimes(
    TimetableSlotUtils.Faculty.FEL,
    ["7:30", "8:15", "9:15", "10:00", "11:00", "11:45", "12:45", "13:30", "14:30", "15:15", "16:15", "17:00", "18:00", "18:45", "19:45"],
    ["7:30", "8:15", "9:00", "10:00", "10:45", "11:45", "12:30", "13:30", "14:15", "15:15", "16:00", "17:00", "17:45", "18:45", "19:30"]))

TimetableUtils.add_faculty(TimetableSlotUtils.FacultyTimes(
    TimetableSlotUtils.Faculty.FIT,
    ["7:30", "8:15", "9:15", "10:00", "11:00", "11:45", "12:45", "13:30", "14:30", "15:15", "16:15", "17:00", "18:00", "18:45", "19:45"],
    ["7:30", "8:15", "9:00", "10:00", "10:45", "11:45", "12:30", "13:30", "14:15", "15:15", "16:00", "17:00", "17:45", "18:45", "19:30"]))