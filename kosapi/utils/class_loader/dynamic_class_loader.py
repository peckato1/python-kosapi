import importlib


class DynamicClassLoader:
    @classmethod
    def load(cls, package=None, name=None):
        mod = importlib.import_module(package)
        return getattr(mod, name)

    @classmethod
    def load_full(cls, path):
        mod = importlib.import_module(path[0])
        return getattr(mod, path[1])
