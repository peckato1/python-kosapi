from kosapi.utils import visitor
from kosapi.entity.fields import BaseMetaField
from kosapi.settings import XML_NAMESPACES as NSMAP


class XMLMetaFieldParserVisitor(visitor.Visitor):
    VISITED_CLASS = BaseMetaField

    def visit_UpdatedField(self, field, etree_node):
        return field.from_string(etree_node.text)

    def visit_TitleField(self, field, etree_node):
        return field.from_string(etree_node.text)

    def visit_IdField(self, field, etree_node):
        return field.from_string(etree_node.text)

    def visit_AuthorField(self, field, etree_node):
        name_node = etree_node.find('atom:name', NSMAP)
        return field.from_string(name_node.text)

    def visit_LinkField(self, field, etree_node):
        return etree_node.get('href')