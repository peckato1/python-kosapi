import re


def strip_xml_encoding(text):
    """Removes information about UTF-8 encoding from a (XML) string"""
    XML_ENCODING_STR = '<?xml version="1.0" encoding="UTF-8"?>\n'
    if text.startswith(XML_ENCODING_STR):
        return text[len(XML_ENCODING_STR):]
    else:
        return text


def remove_namespace_from_string(text, nsmap):
    """Removes namespace prefix from text. Namespace prefixes are passed in nsmap."""
    for k, v in nsmap.items():
        text = re.sub("^{%s}" % v, "", text)
    return text


def get_attr_namespace(tree, attr, ns, nsmap):
    """Gets attribute prefixed with namespace"""
    return tree.get("{%s}%s" % (nsmap[ns], attr), None)
