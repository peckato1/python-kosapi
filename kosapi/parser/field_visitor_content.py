from kosapi.utils import visitor
from kosapi.entity.fields import BaseField, XLinkFieldProxy
from kosapi.entity_factory import EntityFactory
from kosapi.parser.lxml import get_attr_namespace, remove_namespace_from_string
from kosapi.settings import XML_NAMESPACES as NSMAP


class XMLFieldParserVisitor(visitor.Visitor):
    VISITED_CLASS = BaseField

    def visit_IntegerField(self, field, etree_node):
        return field.from_string(etree_node.text)

    def visit_BooleanField(self, field, etree_node):
        return field.from_string(etree_node.text)

    def visit_StringField(self, field, etree_node):
        return field.from_string(etree_node.text)

    def visit_DateField(self, field, etree_node):
        return field.from_string(etree_node.text)

    def visit_DateTimeField(self, field, etree_node):
        return field.from_string(etree_node.text)

    def visit_EnumField(self, field, etree_node):
        enum_cls = field._type
        return enum_cls[etree_node.text]

    def visit_EntityField(self, field, etree_node):
        from kosapi.parser.entity_loader import EntityLoader
        entity = EntityFactory.create_instance(field._type)
        el = EntityLoader(entity)
        el.load_content(etree_node)
        return el.entity

    def visit_DictField(self, field, etree_node):
        if field._attr[0] is None:
            key = etree_node.get(field._attr[1])
        else:
            key = get_attr_namespace(etree_node, field._attr[1], field._attr[0], NSMAP)

        return key, field._field.from_string(etree_node.text)

    def visit_XLinkField(self, field, etree_node):
        return XLinkFieldProxy(remove_namespace_from_string(etree_node.tag, NSMAP),
                               etree_node.text,
                               get_attr_namespace(etree_node, "href", "xlink", NSMAP))

    def visit_XLinkListField(self, field, etree_node):
        if field._nested is True:
            ret = []
            for child in etree_node:
                ret.append(XLinkFieldProxy(remove_namespace_from_string(child.tag, NSMAP),
                                           child.text,
                                           get_attr_namespace(child, "href", "xlink", NSMAP)))
            return ret
        else:
            return [XLinkFieldProxy(remove_namespace_from_string(etree_node.tag, NSMAP),
                                    etree_node.text,
                                    get_attr_namespace(etree_node, "href", "xlink", NSMAP))]
