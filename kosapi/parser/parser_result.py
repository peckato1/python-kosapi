class ParserResult:
    def __init__(self, entities, paginated, next_link=None):
        self._entities = entities
        self._next = next_link
        self._paginated = paginated

    @property
    def next_url(self):
        return self._next

    @property
    def entities(self):
        return self._entities