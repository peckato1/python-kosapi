import lxml.etree
import lxml.objectify

from kosapi.entity_factory import EntityFactory
from kosapi.parser.lxml import strip_xml_encoding, remove_namespace_from_string, get_attr_namespace
from kosapi.parser.parser_result import ParserResult
from kosapi.parser.entity_loader import EntityLoader
from kosapi.settings import XML_NAMESPACES as NSMAP


class XMLParser:
    def __init__(self, input_text):
        self.xml_tree = lxml.etree.fromstring(strip_xml_encoding(input_text))

    def parse(self):
        paginated = self._paginated()
        next_link = self._parse_next_page_link() if paginated else None
        entries = self._parse_entries()

        return ParserResult(entries, paginated, next_link=next_link)

    def _paginated(self):
        root_tag = remove_namespace_from_string(self.xml_tree.tag, NSMAP)

        if root_tag == 'entry':
            return False
        elif root_tag == 'feed':
            return True
        else:
            raise ValueError

    def _parse_next_page_link(self):
        next_node = self.xml_tree.find('atom:link[@rel="next"]', namespaces=NSMAP)
        return next_node.get('href') if next_node is not None else None

    def _parse_entries(self):
        # atom:entry
        #   -> atom:author
        #       -> atom:name
        #   -> atom:id
        #   -> atom:title
        #   -> atom:updated
        #   -> atom:link rel=self
        #   -> atom:content

        nodes_entry = self.xml_tree.xpath('.//atom:entry|/atom:entry', namespaces=NSMAP)
        entries = list()

        for entry in nodes_entry:
            content = entry.find('atom:content', namespaces=NSMAP)

            xsi_type = get_attr_namespace(content, 'type', 'xsi', NSMAP)
            entity = EntityFactory.create_instance_xsi(xsi_type)

            loader = EntityLoader(entity)
            loader.load_meta(entry)
            loader.load_content(content)
            entries.append(loader.entity)

        return entries