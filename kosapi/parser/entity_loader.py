from kosapi.parser.lxml import get_attr_namespace, remove_namespace_from_string
from kosapi.settings import XML_NAMESPACES as NSMAP

from kosapi.parser.field_visitor_content import XMLFieldParserVisitor
from kosapi.parser.field_visitor_meta import XMLMetaFieldParserVisitor


class EntityLoader:
    def __init__(self, entity):
        self._entity = entity

    @property
    def entity(self):
        return self._entity

    def load_meta(self, xml_entry_node):
        visitor_obj = XMLMetaFieldParserVisitor()

        for node in ['atom:title', 'atom:id', 'atom:updated', 'atom:link', 'atom:author']:
                xml_node = xml_entry_node.find(node, namespaces=NSMAP)

                if xml_node is not None:
                    key = node.split(':')[1] # Get tag from 'NS:tag'

                    try:
                        self._entity[key] = self._entity.field_descriptor(key).accept(visitor_obj, xml_node)
                    except KeyError:
                        raise AttributeError("Entity does not have meta field '%s'" % key)
                    except ValueError:
                        raise

    def load_content(self, xml_node):
        visitor_obj = XMLFieldParserVisitor()

        for child in xml_node:
            # etree.cleanup_namespaces() does not work for me
            key = remove_namespace_from_string(child.tag, NSMAP)

            try:
                self._entity[key] = self._entity.field_descriptor(key).accept(visitor_obj, child)
            except KeyError:
                raise AttributeError("Entity '%s' does not have field '%s'" % (type(self.entity), key))
            except ValueError:
                raise