from setuptools import setup, find_packages


setup(
    name='kosapi',
    packages=find_packages(),
    version='1',
    license='MIT',
    description='Python bindings for KOSAPI',
    author='Tomas Pecka',
    author_email='peckato1@fit.cvut.cz',
    keywords=[],
    classifiers=[],
    install_requires=["lxml", "requests_oauthlib"]
)
